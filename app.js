// one
// получить значение true

const a = 3 !== 2
console.log(a)

// two
// поменять местами две переменные

let a2 = 15
let a3 = "мама"
const swap = a2
a2 = a3
a3 = swap
console.log(a2)
console.log(a3)

// three
// получить значение NaN

const c = 2 - "мама"
console.log(c)

// four
// решить пример и записать только остаток

const d2 = 2
const d = ((d2 + 2) ** 3) / 6 + 4.5
const d3 = d % 3
console.log(d3)

// five
// перевести строку в число

const z = Number("55")
console.log(z)

// six
// значение true

const v = z > d3
console.log(v)